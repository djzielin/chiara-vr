﻿using UnityEngine;
using System.Collections;

public class TextDisplayer : MonoBehaviour 
{
	TextMesh tm;
	bool displayText=false;
	float elapsedtime=0.0f;
	float timeText=4.0f;

	// Use this for initialization
	void Start () {
		tm = GetComponent<TextMesh> ();

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (displayText)
		{
			elapsedtime = elapsedtime + Time.deltaTime;
			if (elapsedtime > timeText)
			{
				tm.text = "";
				displayText = false;
			}
		}

	}

	public void ShowText(string s)
	{
		tm.text = s;
		elapsedtime = 0.0f;
		displayText = true;
	}
}
﻿using UnityEngine;
using System.Collections;

public class Switch : MonoBehaviour {
	public GameObject[] ToggleObjects = new GameObject[4];

	bool showFirst;

	// Use this for initialization

	void SetupFirstOnly()
	{
		showFirst = true;
		ToggleObjects[0].SetActive (true);
		ToggleObjects[1].SetActive(true);
		ToggleObjects[2].SetActive(false);
		ToggleObjects[3].SetActive(false);
		print ("first relief setup");
	}

	void SetupSecondOnly()
	{
		showFirst = false;
		ToggleObjects[0].SetActive (false);
		ToggleObjects[1].SetActive(false);
		ToggleObjects[2].SetActive(true);
		ToggleObjects[3].SetActive(true);
		print ("second relief setup");
	}

	void Start () 
	{
		SetupFirstOnly ();
	}

	bool isUserToggling()
	{
		if (MiddleVR.VRDeviceMgr != null)
		{
			return MiddleVR.VRDeviceMgr.IsWandButtonToggled (1);
		} 

		return false;
	}

	// Update is called once per frame
	void Update ()
	{
		//if (Input.GetKeyDown (KeyCode.R)) 
		if(isUserToggling())
		{
			print ("user pressed r");
			if (showFirst)   //if we are showing the first configuration now, lets switch to the 2nd configuration
			{	
				SetupSecondOnly ();
			}
			else  //we must be showing the second configuration now, lets switch to the first
			{          
				SetupFirstOnly ();
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class ToggleChurchComponents : MonoBehaviour {
	public GameObject[] ToggleObjects = new GameObject[2];

	bool showFirst;

	// Use this for initialization

	void SetupFirstOnly()
	{
		showFirst = true;
		ToggleObjects[0].SetActive (true);
		ToggleObjects[1].SetActive(false);
		print ("first view setup");
	}

	void SetupSecondOnly()
	{
		showFirst = false;
		ToggleObjects[0].SetActive (false);
		ToggleObjects[1].SetActive(true);
		print ("second view setup");
	}

	void Start () 
	{
		SetupFirstOnly ();
	}


	bool isUserToggling()
	{
		if (MiddleVR.VRDeviceMgr != null)
		{
			return MiddleVR.VRDeviceMgr.IsWandButtonToggled (0);
		} 

		return false;
	}
		
	// Update is called once per frame
	void Update ()
	{
		//if (Input.GetKeyDown (KeyCode.T)) 
		if(isUserToggling())
		{
			print ("user pressed t");
			if (showFirst)   //if we are showing the first configuration now, lets switch to the 2nd configuration
			{	
				SetupSecondOnly ();
			}
			else  //we must be showing the second configuration now, lets switch to the first
			{          
				SetupFirstOnly ();
			}
		}
	}
}

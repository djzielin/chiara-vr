﻿using UnityEngine;
using System.Collections;

public class React: MonoBehaviour {
	//bool raycastCollision = false;
	TextDisplayer tDisplayer;

	void Start()
	{
		tDisplayer = GetComponent<TextDisplayer> ();
	}

	bool isUserToggling()
	{
		if (MiddleVR.VRDeviceMgr != null)
		{
			return MiddleVR.VRDeviceMgr.IsWandButtonToggled (5);
		} 

		return false;
	}
		
	void Update ()
	{

		//gameObject.GetComponentInChildren<Transform> ();

		//if (Input.GetMouseButtonDown (0))
		if(isUserToggling())
		{
			//Ray userRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			Ray userRay=new Ray();

			GameObject hand = GameObject.Find ("HandNode");

			string totalString = "";

			userRay.origin = hand.transform.position;
			userRay.direction = hand.transform.TransformDirection (Vector3.forward);

			print ("direction: " + userRay.direction);

			//totalString += "Ray Origin: " + userRay.origin + "\n";
			//totalString += "Ray Dir: " + userRay.direction + "\n";

			RaycastHit hit;

			if (Physics.Raycast (userRay, out hit))
			{
				//raycastCollision = true;
				Transform hitTransform = hit.transform;

				GameObject objectWeHit = hit.collider.gameObject;

				print ("we clicked on: " + objectWeHit.name);
				metadata meta = objectWeHit.GetComponent<metadata> ();


				totalString += "object name: " + objectWeHit.name + "\n";

				if (meta != null)
				{

					for (int i = 0; i < meta.keys.Length; i++)
						totalString += meta.keys [i] + "--> " + meta.values [i] + "\n";
				} else
				{
					totalString += "no metadata defined for this object";

				}

			} else
				totalString += "no object hit with raycast\n";

			tDisplayer.ShowText (totalString);
		}
	}
}

